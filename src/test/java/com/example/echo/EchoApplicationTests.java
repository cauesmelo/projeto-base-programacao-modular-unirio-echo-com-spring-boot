package com.example.echo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.echo.webservice.SwapiClient;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EchoApplicationTests {

	@Autowired
	private SwapiClient swapiClient;

	@Test
	void testeSwapiClient() {
		assertEquals("Tatooine", swapiClient.getPlanetName("1"));
	}

}
